'use strict';

module.exports = {
    clean: require('./src/clean.js'),

    jshint: require('./src/jshint.js'),
    jscs: require('./src/jscs.js'),
    jsdoc: require('./src/jsdoc.js'),
    karma: require('./src/karma.js'),
    templates: require('./src/templates.js'),

    phplint: require('./src/phplint.js'),
    phpcs: require('./src/phpcs.js'),
    phpcbf: require('./src/phpcbf.js'),
    phpunit: require('./src/phpunit.js'),
    phpdoc: require('./src/phpdoc.js'),

    module: require('./src/module.js'),

    less: require('./src/less.js'),
    lessmin: require('./src/lessmin.js'),

    bless: require('./src/bless.js'),
    blessmin: require('./src/blessmin.js'),
};
