'use strict';

module.exports = function (config)
{
    return function()
    {
        var gulp = require('gulp');
        var wrap = require('gulp-wrap');
        var ngTemplates = require('gulp-ng-templates');

        return gulp.src(config.src, {base: 'src'})
            .pipe(ngTemplates(config))
            .pipe(wrap("'use strict';\n<%= contents %>"))
            .pipe(gulp.dest(config.dest));
    };
};
