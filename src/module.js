'use strict';

/**
 * gulp|gulp build
 * gulp dev
 */
module.exports = function (gulp, basePath, bower, vendor)
{
    var jshint = require('./jshint.js');
    var jscs = require('./jscs.js');
    var jsdoc = require('./jsdoc.js');
    var karma = require('./karma.js');
    var templates = require('./templates.js');
    var less = require('./less.js');
    var handleError = require('./lib/handle-error.js');
    var cached = require('gulp-cached');
    var remember = require('gulp-remember');

    function forget(cacheName)
    {
        return function (event)
        {
            if (event.type === 'deleted') {
                delete cached.caches[cacheName][event.path];
                remember.forget(cacheName, event.path);
            }
        }
    }

    var config = {
        scripts: [
            'src/**/app.js',
            'src/**/*.app.js',
            'src/**/*.js',
            '!src/**/*Test.js'
        ],
        tests: [
            'tests/**/app.js',
            'tests/**/*.app.js',
            'tests/**/*.js',
            'src/**/*Test.js'
        ]
    };

    ////
    // Javascript
    gulp.task('jshint', jshint(config.scripts));

    gulp.task('jshint-tests', jshint(config.tests, {cache: 'jshint-tests'}));

    gulp.task('jscs', ['jshint'], jscs(config.scripts));

    gulp.task('jsdoc', ['jshint'], jsdoc(['src/']));

    ////
    // Test
    gulp.task('test', ['karma']);

    gulp.task('karma', ['scripts'], karma(vendor.concat([
        basePath + '/dist/' + bower.name + '.js',
    ], config.tests.map(function (pattern) {
        return basePath + '/' + pattern;
    }))));

    ////
    // Build
    gulp.task('default', ['build']);
    gulp.task('build', ['less', 'scripts', 'jscs', 'jsdoc', 'test']);

    gulp.task('less', less('src/styles/main.less', 'dist/' + bower.name + '.css'));

    gulp.task('templates', templates({
        src: 'src/**/*.html',
        dest: 'dist/',
        filename: 'templates.js',
        module: bower.name + '.templates',
        standalone: true,
        path: function(path, base) {
            return path.replace(new RegExp('^.*' + base), bower.name);
        }
    }));

    gulp.task('scripts', ['templates'], function()
    {
        var wrap = require('gulp-wrap');
        var concat = require('gulp-concat');
        var sourcemaps = require('gulp-sourcemaps');
        var ngAnnotate = require('gulp-ng-annotate');
        var babel = require('gulp-babel');
        var removeUseStrict = require("gulp-remove-use-strict");

        return gulp.src(config.scripts.concat(
                'dist/templates.js'
            ))
            .pipe(sourcemaps.init())
            .pipe(cached('scripts'))
            .pipe(babel({
                presets: ['es2015'],
            }))
            .on('error', handleError)
            .pipe(removeUseStrict())
            .on('error', handleError)
            .pipe(ngAnnotate())
            .on('error', handleError)
            .pipe(remember('scripts'))
            .pipe(concat(bower.name + '.js'))
            .pipe(wrap({
                src: __dirname + '/lib/wrap.txt'
            }))
            .pipe(sourcemaps.write('.', {sourceRoot: '.'}))
            .pipe(gulp.dest('dist/'));
    });

    ////
    // Dev
    gulp.task('dev', ['server', 'livereload', 'watch']);

    gulp.task('server', function()
    {
        var express = require('express');
        var app = express();

        app.use(express.static('.'));

        var server = app.listen(8000, function () {
            var port = server.address().port;

            console.log('Express listening at on port %s', port);
        });
    });

    gulp.task('livereload', function()
    {
        var livereload = require('livereload');
        var server = livereload.createServer({exts: ['css']});
        server.watch(__dirname);
        console.log('Livereload running...');
    });

    gulp.task('watch', function()
    {
        gulp.watch('src/**/*.less', ['less']);

        gulp.watch(config.scripts, ['jshint', 'scripts'])
            .on('change', forget('scripts'))
            .on('change', forget('jshint'));

        gulp.watch('src/**/*.html', ['scripts']);

        gulp.watch(config.tests, ['jshint-tests', 'karma'])
            .on('change', forget('jshint-tests'));
    });
};
