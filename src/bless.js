'use strict';

module.exports = function (cssSrc, outDir)
{
    var exec = require('child_process').exec;

    return function(done)
    {
        exec('node_modules/.bin/blessc chunk ' + cssSrc + ' --out-dir ' + outDir, done);
    };
};


