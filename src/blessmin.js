'use strict';

module.exports = function (cssSrc, outDir)
{
    var exec = require('child_process').exec;

    return function(done)
    {
        var bless = require('./bless.js');

        bless(cssSrc, outDir)(function ()
        {
            var glob = require('glob');
            var path = require('path');
            var wildcard = path.basename(cssSrc).replace('.css', '*.css');
            wildcard = outDir + '/' + wildcard;

            glob(wildcard, null, function(error, files)
            {
                if (error) {
                    throw error;
                }

                if (files.length === 0) {
                    throw new Error('Files for bless to minify are not found');
                }

                var command = 'node_modules/.bin/lessc --clean-css @file @file';
                var execCommand = '';

                for (var index in files) {
                    execCommand += execCommand ? ' & ': '';
                    execCommand += command.replace(new RegExp('@file', 'g'), files[index]);
                }

                exec(execCommand, done);
            });

        });
    };
};


