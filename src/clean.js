'use strict';

module.exports = function (src)
{
    return function ()
    {
        var gulp = require('gulp');
        var rimraf = require('gulp-rimraf');

        return gulp.src(src, {read: false})
            .pipe(rimraf());
    };
};
