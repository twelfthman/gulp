'use strict';

module.exports = function (src)
{
    return function (cb)
    {
        var karmaServer = require('karma').Server;

        new karmaServer({
            configFile: __dirname + '/config/karma.conf.js',
            files: src,
            singleRun: true
        }, cb).start();
    };
};
