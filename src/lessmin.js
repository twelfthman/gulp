'use strict';

module.exports = function (src, dest)
{
    return function(done)
    {
        var less = require('./less.js');

        less(src, dest, true)(function()
        {
            var rev = require('rev-hash');
            var fs = require('fs');
            var buffer = fs.readFileSync(dest);
            var hash = rev(buffer);
            var newDest = dest.replace(/\.css$/, '.' + hash + '.css');

            fs.renameSync(dest, newDest);
            done(newDest);
        });
    };
};
