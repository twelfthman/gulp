'use strict';

module.exports = function(src)
{
    return function (cb)
    {
        var fs = require('fs');
        var exec = require('child_process').exec;

        if (!fs.existsSync('./vendor/bin/phpcbf')) {
            console.log('phpcbf is not installed: composer require squizlabs/php_codesniffer');
            return;
        }

        exec('./vendor/bin/phpcbf --standard=PSR2 ' + src.join(' '), cb);
    };
};
