'use strict';

module.exports = function (src)
{
    return function()
    {
        var temp = '.gulp/phpcs';
        var fs = require('fs');
        var gulp = require('gulp');
        var phpcs = require('gulp-phpcs');
        var changed = require('gulp-changed');
        var through = require('through2');
        var gutil = require('gulp-util');
        var notify = require('gulp-notify');
        var PluginError = gutil.PluginError;

        if (!fs.existsSync('./vendor/bin/phpcs')) {
            throw new PluginError('twelfthman-gulp', 'phpcs missing: composer require squizlabs/php_codesniffer');
        }

        return gulp.src(src)
            .pipe(changed(temp))
            .pipe(phpcs({
                bin: 'vendor/bin/phpcs',
                standard: 'PSR2',
                warningSeverity: 0
            }))
            .pipe(phpcs.reporter('log'))
            .pipe(through.obj(function(file, enc, cb) {

                // Only pass through successful files
                if (file.phpcsReport.error) {
                    // file.phpcsReport.output
                    this.emit('error', new PluginError('twelfthman-gulp', 'PHP Code Standards Failed'));
                }

                this.push(file);
                cb();
            }))
            .on('error', notify.onError("<%= error.message %>"))
            .pipe(gulp.dest(temp));
    }
};
