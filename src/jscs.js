'use strict';

module.exports = function (src, temp)
{
    return function ()
    {
        var gulp = require('gulp');
        var jscs = require('gulp-jscs');
        var cached  = require('gulp-cached');

        return gulp.src(src, {base: '.'})
            .pipe(cached('jscs'))
            .pipe(jscs({
                fix: true,
                configPath: __dirname + '/config/jscs.json'
            }))
            .pipe(gulp.dest('.'));
    };
};
